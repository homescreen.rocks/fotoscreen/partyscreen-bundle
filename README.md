# partyscreen-bundle

## Usage

```
docker run \
    -v partyscreen:/data \
    -e INSTAGRAM_USERNAME=foo \
    -e INSTAGRAM_PASSWORD=bar \
    registry.gitlab.com/homescreen.rocks/partyscreen-bundle/master:latest
```
