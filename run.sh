#!/bin/sh

set -exuo pipefail

exec partyscreen-backend \
    --ui-directory /src \
    --data-directory /data \
    --instagram-username ${INSTAGRAM_USERNAME:-} \
    --instagram-password ${INSTAGRAM_PASSWORD:-} \
    -v
